import { combineReducers } from 'redux';
import dashBoardReducer from '../dashboard/dashboardReducer';
import tabReducer from '../common/tabs/tabReducer';
import BillingCycleReducer from '../billingCycle/BillingCycleReducer'
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';

const rootReducer = combineReducers({
    dashboard: dashBoardReducer,
    tab: tabReducer,
    billingCycle: BillingCycleReducer,
    form: formReducer,
    toastr: toastrReducer
});

export default rootReducer;