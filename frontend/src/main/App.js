import '../common/template/dependecies';
import React from 'react';
import Header from '../common/template/Header';
import SideBar from '../common/template/SideBar';
import Footer from '../common/template/Footer';
import Routes from './Routes';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import multi from 'redux-multi';
import promise from 'redux-promise';
import rootReducer from './rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import Messages from '../common/msg/Messages';

const store = createStore(rootReducer,composeWithDevTools(applyMiddleware(thunk,multi,promise)));
const App = props => (
    <Provider store={store}>
        <div className='wrapper'>
            <Header />
            <SideBar />
            <div className='content-wrapper'>
                <Routes />
            </div>
            <Footer />
            <Messages />
        </div>
    </Provider>
)

export default App;