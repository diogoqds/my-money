export const URL = 'http://localhost:4000/api';
export const BILLING_SUMMARY_FETCHED = 'BILLING_SUMMARY_FETCHED';
export const TAB_SELECTED = 'TAB_SELECTED';
export const TAB_SHOWED = 'TAB_SHOWED';
export const BILLING_CYCLES_FETCHED = 'BILLING_CYCLES_FETCHED'