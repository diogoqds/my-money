import axios from 'axios';
import {
    URL,
    BILLING_SUMMARY_FETCHED
} from '../main/consts';

export function getSummary() {
    return dispatch => {
        axios.get(`${URL}/billingCycles/summary`)
        .then(response => {
            dispatch({type:BILLING_SUMMARY_FETCHED, payload:response.data})
        })
    }
}