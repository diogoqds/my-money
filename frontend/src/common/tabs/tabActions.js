import {
    TAB_SELECTED,
    TAB_SHOWED
} from '../../main/consts'

export function selectTab(tabId) {
    return {
        type: TAB_SELECTED,
        payload: tabId
    }
}

export function showTabs(...tabsIds) {
    const tabToShow = {}
    tabsIds.forEach(element => tabToShow[element] = true)
    return {
        type: TAB_SHOWED,
        payload: tabToShow
    }
}