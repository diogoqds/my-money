import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getList, showUpdate, showDelete } from './BillingCycleActions'

class BillingCycleList extends Component {
    componentWillMount() {
        this.props.getList()
    }

    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Mês</th>
                            <th>Ano</th>
                            <th className='table-actions'>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderList()}
                    </tbody>
                </table>
            </div>
        )
    }

    renderList() {
        return this.props.list.map(billingCycle => (
            <tr key={billingCycle._id}>
                <td>{billingCycle.name}</td>
                <td>{billingCycle.month}</td>
                <td>{billingCycle.year}</td>
                <td>
                    <button className='btn btn-warning' onClick={() => this.props.showUpdate(billingCycle)}>
                        <i className='fa fa-pencil' />
                    </button>
                    <button className='btn btn-danger' onClick={() => this.props.showDelete(billingCycle)}>
                        <i className='fa fa-trash-o' />
                    </button>
                </td>
            </tr>
        ))
    }
}

const mapStateToProps = state => (
    {
        list: state.billingCycle.list
    }
)

const mapDispatchToProps = dispatch => (
    bindActionCreators({getList,showUpdate, showDelete},dispatch)
)
export default connect(mapStateToProps,mapDispatchToProps)(BillingCycleList)