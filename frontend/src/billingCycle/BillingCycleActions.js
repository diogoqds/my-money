import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import { reset as resetForm, initialize } from 'redux-form';
import { selectTab, showTabs } from '../common/tabs/tabActions';
import {
    URL,
    BILLING_CYCLES_FETCHED
} from '../main/consts';

const INITIAL_VALUES = { credits:[{}], debts:[{}] }

export function getList() {
    return dispatch => {
        axios.get(`${URL}/billingCycles`)
        .then(response => 
            dispatch({
                type: BILLING_CYCLES_FETCHED,
                payload: response.data
            })
        )
    }
}

export function create(values) {
    return submit(values,'post')
}

export function update(values) {
    return submit(values,'put')
}

export function destroy(values) {
    return submit(values,'delete')
}

function submit(values,method) {
    return dispatch => {
        const id = values._id ? values._id : '';
        axios[method](`${URL}/billingCycles/${id}`,values)
        .then(response => {
            toastr.success('Sucesso','Operação realizada com sucesso!')
            dispatch(init())
        })
        .catch(error => {
            error.response.data.errors.forEach(error => toastr.error('Erro',error))
        })
    }
}


export function showUpdate(billingCycle) {
    return [
        showTabs('tabUpdate'),
        selectTab('tabUpdate'),
        initialize('billingCycleForm',billingCycle)
    ]
}

export function showDelete(billingCycle) {
    return [
        showTabs('tabDelete'),
        selectTab('tabDelete'),
        initialize('billingCycleForm',billingCycle)
    ]
}

export function init() {
    return [
        showTabs('tabList','tabCreate'),
        selectTab('tabList'),
        getList(),
        initialize('billingCycleForm',INITIAL_VALUES)
    ]
}