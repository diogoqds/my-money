import React, { Component } from 'react';
import ContentHeader from '../common/template/ContentHeader';
import Content from '../common/template/Content';
import Tabs from '../common/tabs/Tabs';
import TabsContent from '../common/tabs/TabsContent';
import TabsHeader from '../common/tabs/TabsHeader';
import TabHeader from '../common/tabs/TabHeader';
import TabContent from '../common/tabs/TabContent';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BillingCycleList from './BillingCycleList';
import BillingCycleForm from './BillingCycleForm';
import { init,create, update, destroy } from './BillingCycleActions';

class BillingCycle extends Component {

    componentWillMount() {
        this.props.init()
    }

    render() {
        return (
            <div>
                <ContentHeader title='Ciclos de Pagamentos' small='Cadastro' />
                <Content>
                    <Tabs>
                        <TabsHeader>
                            <TabHeader label='Listar' icon='bars' target='tabList' />
                            <TabHeader label='Incluir' icon='plus' target='tabCreate' />
                            <TabHeader label='Alterar' icon='pencil' target='tabUpdate' />
                            <TabHeader label='Excluir' icon='trash-o' target='tabDelete' />
                        </TabsHeader>
                        <TabsContent>
                            <TabContent id='tabList'>
                                <BillingCycleList />
                            </TabContent>
                            <TabContent id='tabCreate'>
                                <BillingCycleForm onSubmit={this.props.create}
                                    submitLabel='Criar'
                                    submitClass='primary'
                                    />
                            </TabContent>
                            <TabContent id='tabUpdate'>
                                <BillingCycleForm onSubmit={this.props.update}
                                    submitLabel='Atualizar'
                                    submitClass='info'
                                    />
                            </TabContent>
                            <TabContent id='tabDelete'>
                                <BillingCycleForm onSubmit={this.props.destroy} readOnly={true}
                                    submitLabel='Excluir'
                                    submitClass='danger'
                                />
                            </TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}



const mapDispatchToProps = dispatch => ( 
    bindActionCreators({init, create, update, destroy},dispatch)
)
export default connect(null,mapDispatchToProps)(BillingCycle);